// TODO TODAY:
// * flatten line with char
// * 3 font-sizes / width-height of filter *2 or /2
// * transparent bg mode
// * color mode: [bw, colored-pp(?), colored-logo]

// init elements
let root = document.documentElement;
let filters_container = document.getElementById("filters");
let slides_container = document.getElementById("slides-container");
let slide_template = document.getElementById("slide--template");
let canvases = document.getElementsByTagName("canvas");
const eu_logo = document.getElementById("eu-logo");

let to_draw = {
  mode: "",
  format: "",
  image_filters: [],
  img: undefined,
  eu_logo: eu_logo,
  title_xy: {
    x: 0,
    y: 0,
  },

  partners: [],

  // canvas variable
  fontSize: 108,
  backgroundColor: "#f0f0f0",
  // spacing
  margin: 48,
  partner_color: 16,
};
let colors = {
  tekhne: "#000000",
  "ctm-festival": "#ff0000",
  gmea: "#00ff00",
  "out-ra": "#0000ff",
  "q-o2": "#00ffff",
  "skanu-mezs": "#ff00ff",
  trafo: "#ffff00",
};
let valid_filters = [
  // diy filters made by Einar
  "custom1",
  "custom2",
  "custom3",
  "custom4",

  // a selection of filter for black and white typography :)
  "f060",
  "f077",
  "f164",
  "f006",
  "f007",
  "f094",
  // 'f066', removing peel off because lisibility problems
  "f068",
  "f102",
  "f108",
  "f144",
  "f057",
  "f227",
  "f231",
];

let cached_logo = {
  text: "",
  format: to_draw.format,
  canvas: document.createElement("canvas"),
  width: 0,
  height: 0,
  x: 0,
  y: 0,
};

//  DRAW FUNCTIONS
//  -------------------------------------------------------------------

function draw_filter(context, canvas) {
  if (to_draw.image_filters.length > 0) {
    const image_filter_string = to_draw.image_filters.reduce((accumulator, currentValue) => accumulator + `url(#${currentValue}) `,
      "",)
    context.filter = image_filter_string;
  } else {
    context.filter = "";
  }
}
function draw_image(context, canvas) {
  if (to_draw["img"] !== undefined) {
    const img = to_draw["img"];
    // const extraBottomPadding = to_draw.eu_logo || to_draw['tekhne_logo'] ? 97 : to_draw.partner_color // in case we want the image to be centered between the logos
    const extraBottomPadding = to_draw.partner_color
    const drawingAreaHeight =
      canvas.height - to_draw.title_xy.y - extraBottomPadding;
    const scaleRatio = Math.min(
      (canvas.width - 2 * to_draw.margin) / img.width,
      (drawingAreaHeight - 2 * to_draw.margin) / img.height
    ) * to_draw.imgSize
    const x = canvas.width / 2 - (img.width * scaleRatio) / 2;
    const y =
      (canvas.height - to_draw.title_xy.y - extraBottomPadding) / 2 -
      (img.height * scaleRatio) / 2 +
      to_draw.title_xy.y;
    context.drawImage(
      img,
      x,
      y,
      img.width * scaleRatio,
      img.height * scaleRatio
    );
  }
}

function draw_eu_logo(context, canvas) {
  if (to_draw["eu_logo"] !== undefined) {
    const img = to_draw["eu_logo"];
    // const desiredWidth = 250;
    // const scaleRatio = desiredWidth / img.width;
    //
    const x = to_draw["margin"];
    const y = canvas.height - to_draw["margin"] - img.height;
    // context.drawImage(img, x, y, img.width * scaleRatio, img.height * scaleRatio);
    context.drawImage(img, x, y);
  }
}

function draw_partner_color(context, canvas) {
  const rec_w = canvas.width / to_draw["partners"].length;
  const rec_h = to_draw["partner_color"];
  let i = 0;

  for (partner of to_draw["partners"]) {
    context.fillStyle = colors[partner.id];
    context.fillRect(i * rec_w, canvas.height - rec_h, rec_w, rec_h);
    i = i + 1;
  }
}
function getLines(ctx, text, maxWidth) {
  // borrowed from stackoverflow
  // https://stackoverflow.com/questions/2936112/text-wrap-in-a-canvas-element
  const words = text.split(" ");
  let lines = [];
  let currentLine = words[0];

  for (let i = 1; i < words.length; i++) {
    let word = words[i];
    let width = ctx.measureText(currentLine + " " + word).width;
    if (width < maxWidth) {
      currentLine += " " + word;
    } else {
      lines.push(currentLine);
      currentLine = word;
    }
  }
  lines.push(currentLine);
  return lines;
}

function draw_write(type, context, canvas) {
  let text = to_draw[type];
  let x, y, fs;

  if (type == "title") {
    context.textBaseline = "top";
    fs = 72;
    x = to_draw["margin"];
    y = to_draw["margin"];
  }
  if (type == "text") {
    context.textBaseline = "top";
    fs = 36;
    x = to_draw["margin"];
    y = to_draw.margin + to_draw.title_xy.y;
  }
  if (type == "sup_text_tl") {
    context.textBaseline = "top";
    context.textAlign = "left";
    fs = 36;
    x = canvas.width / 2;
    x = to_draw["margin"];
    y = to_draw["margin"];
  }
  if (type == "sup_text_br") {
    context.textBaseline = "bottom";
    context.textAlign = "right";
    fs = 36;
    x = canvas.width - to_draw["margin"];
    y = canvas.height - to_draw["margin"];
  }

  const lineHeight = fs * 1.2;
  context.font = `${fs}px AmiamieRounded`;
  const splitText = getLines(
    context,
    text,
    canvas.width - 2 * to_draw["margin"]
  );

  if (type === "title") {
    if (text) {
      // Draw a grey background for the title
      const rectHeight =
        splitText.length * fs +
        fs * 0.2 * (splitText.length - 1) +
        2 * to_draw["margin"];
      context.fillStyle = "#f0f0f0";
      context.fillRect(0, 0, canvas.width, rectHeight);
      context.fillStyle = "#CCCCCC";
      const lineHeight = 4;
      // The rectheight is the line number * with the fontsize PLUS the extra margin that the lineheight introduces plut the canvas margin
      context.fillRect(0, rectHeight, canvas.width, lineHeight);
      to_draw.title_xy = {
        x: 0,
        y: rectHeight + lineHeight,
      };
    } else {
      // TODO: The image is drawn before the text so if you go from 1 to 0 characters the image is still placed wrong.
      // this is actually not the place in the code where you would fix this but here is the todo :-)
      to_draw.title_xy = {
        x: 0,
        y: 0,
      };
    }
  }

  context.fillStyle = "black";
  splitText.forEach((line, index) => {
    context.fillText(line, x, y + index * lineHeight);
  });
}

function pick_filters(text) {
  let filters_temp = Array.from(valid_filters);
  let filter_ids = [];
  for (let i = 0; i < text.length; i++) {
    let random = Math.floor(Math.random() * filters_temp.length);
    let pick = filters_temp[random];
    filters_temp.splice(random, 1);
    filter_ids.push(pick);
  }
  console.log("picking new filters", filter_ids);
  return filter_ids;
}

function measure_char_height(char, context) {
  const { fontBoundingBoxAscent, fontBoundingBoxDescent } =
    context.measureText(char);
  return fontBoundingBoxAscent + Math.abs(fontBoundingBoxDescent);
}
function compareArrays(arr1, arr2) {
  // Tim Down: http://stackoverflow.com/a/7837725/308645
  let i = arr1.length;
  if (i !== arr2.length) return false;
  while (i--) {
    if (arr1[i] !== arr2[i]) return false;
  }
  return true;
}

function is_cached_logo_valid() {
  //TODO: Check for filter intensity changes.
  if (to_draw["mode"] === "post") {
    if (cached_logo.fontSize !== 36) {
      return false;
    }
    if (cached_logo.format !== to_draw.format) {
      return false;
    }
    return true;
  } else {
    if (cached_logo.text !== to_draw["logo"]) {
      return false;
    }
    if (cached_logo.format !== to_draw.format) {
      return false;
    }
    if (cached_logo.fontSize !== to_draw.fontSize) {
      return false;
    }
    if (cached_logo.filterScale !== to_draw.filterScale) {
      return false;
    }
    return compareArrays(cached_logo.filters, to_draw.filters);
  }
}

function draw_logo(canvas, context) {
  if (is_cached_logo_valid()) {
    context.drawImage(cached_logo.canvas, cached_logo.x, cached_logo.y);
  } else {
    console.log("logo redrawn");
    text = to_draw["logo"];

    // set text options
    const fs = to_draw["fontSize"];
    context.textBaseline = "alphabetic";
    context.textAlign = "left";
    context.font = `${fs}px AmiamieRounded-bold`; // Set the fontsize so the text can be measured
    const stroke_width = fs * 0.035;
    const padding = 20;
    const gap_multiplier = 1;

    const text_width = text.split("").reduce((accumulator, currentValue) => {
      const { fontBoundingBoxAscent, width } =
        context.measureText(currentValue);
      const gap = fontBoundingBoxAscent * gap_multiplier;

      tempwidth = Math.floor(width + gap);

      return accumulator + tempwidth;
    }, 0);
    const text_height = measure_char_height(text.split("")[0], context);

    const cached_canvas = document.createElement("canvas");
    const cached_ctx = cached_canvas.getContext("2d");
    cached_canvas.width = text_width + 2 * padding;
    cached_canvas.height = text_height + 2 * padding;

    let x_pos = padding;

    for (let i = 0; i < text.length; i++) {
      const char = text[i];
      const { fontBoundingBoxAscent, fontBoundingBoxDescent, width } =
        context.measureText(char);
      const gap = fontBoundingBoxAscent * gap_multiplier;
      const char_width = width;

      // create dummy canvas
      const temp_canvas = document.createElement("canvas");
      temp_canvas.width = char_width + gap;
      temp_canvas.height =
        fontBoundingBoxAscent + Math.abs(fontBoundingBoxDescent);
      const temp_context = temp_canvas.getContext("2d");

      // draw character and stroke on this new "layer"
      temp_context.textBaseline = "middle";
      temp_context.textAlign = "left";
      temp_context.fillStyle = "black";
      temp_context.font = `${fs}px AmiamieRounded-bold`;
      temp_context.fillText(char, 0, temp_canvas.height / 2);
      temp_context.fillRect(
        0,
        temp_canvas.height / 2 - stroke_width / 2,
        temp_canvas.width,
        stroke_width
      );

      // Draw the temporary character onto the cached canvas
      cached_ctx.filter = `url(#${to_draw["filters"][i]}) grayscale(1)`;
      cached_ctx.drawImage(
        temp_canvas,
        x_pos,
        cached_canvas.height / 2 - temp_canvas.height / 2
      );

      x_pos += temp_canvas.width;
    }
    const logo_x = canvas.width / 2 - cached_canvas.width / 2;
    const logo_y = canvas.height / 2 - cached_canvas.height / 2;
    // Update the cache
    cached_logo = {
      text: text,
      format: to_draw.format,
      fontSize: fs,
      filters: to_draw.filters,
      filterScale: to_draw.filterScale,
      canvas: cached_canvas,
      width: cached_canvas.width,
      height: cached_canvas.height,
      x: logo_x,
      y: logo_y,
    };
    context.drawImage(cached_canvas, logo_x, logo_y);
  }
}

function draw_partner_logo(canvas, context) {
  if (is_cached_logo_valid()) {
    context.drawImage(cached_logo.canvas, cached_logo.x, cached_logo.y);
  } else {
    console.log("new partner logo");
    const text = "tekhnē";

    to_draw["filters"] = pick_filters("tekhnē");

    // set text options
    const fs = 36;
    context.textBaseline = "alphabetic";
    context.textAlign = "left";
    context.font = `${fs}px AmiamieRounded-bold`; // Set the fontsize so the text can be measured
    const stroke_width = fs * 0.035;
    const padding = 20;
    const gap_multiplier = 1;

    const text_width = text.split("").reduce((accumulator, currentValue) => {
      const { fontBoundingBoxAscent, width } =
        context.measureText(currentValue);
      const gap = fontBoundingBoxAscent * gap_multiplier;
      tempwidth = Math.floor(width + gap);

      return accumulator + tempwidth;
    }, 0);
    const text_height = measure_char_height(text.split("")[0], context);

    const cached_canvas = document.createElement("canvas");
    const cached_ctx = cached_canvas.getContext("2d");
    cached_canvas.width = text_width + 2 * padding;
    cached_canvas.height = text_height + 2 * padding;

    let x_pos = padding;

    for (let i = 0; i < text.length; i++) {
      const char = text[i];
      const { fontBoundingBoxAscent, fontBoundingBoxDescent, width } =
        context.measureText(char);
      const gap = fontBoundingBoxAscent * gap_multiplier;
      const char_width = width;

      // create dummy canvas
      const temp_canvas = document.createElement("canvas");
      temp_canvas.width = char_width + gap;
      temp_canvas.height =
        fontBoundingBoxAscent + Math.abs(fontBoundingBoxDescent);
      const temp_context = temp_canvas.getContext("2d");

      // draw character and stroke on this new "layer"
      temp_context.textBaseline = "middle";
      temp_context.textAlign = "left";
      temp_context.fillStyle = "black";
      temp_context.font = `${fs}px AmiamieRounded-bold`;
      temp_context.fillText(char, 0, temp_canvas.height / 2);
      temp_context.fillRect(
        0,
        temp_canvas.height / 2 - stroke_width / 2,
        temp_canvas.width,
        stroke_width
      );

      // Draw the temporary character onto the cached canvas
      cached_ctx.filter = `url(#${to_draw["filters"][i]}) grayscale(1)`;
      cached_ctx.drawImage(
        temp_canvas,
        x_pos,
        cached_canvas.height / 2 - temp_canvas.height / 2
      );

      x_pos += temp_canvas.width;
    }
    const logo_x =
      canvas.width - to_draw["margin"] - cached_canvas.width + padding;
    const logo_y =
      canvas.height - to_draw["margin"] - cached_canvas.height + padding;
    // Update the cache
    cached_logo = {
      text: text,
      format: to_draw.format,
      filterScale: to_draw.filterScale,
      fontSize: fs,
      canvas: cached_canvas,
      width: cached_canvas.width,
      height: cached_canvas.height,
      x: logo_x,
      y: logo_y,
    };
    context.drawImage(cached_canvas, logo_x, logo_y);
  }
}

function draw_post(canvas, context) {
  // --- grey background
  context.fillStyle = "#f0f0f0";
  context.fillRect(0, 0, canvas.width, canvas.height);

  // --- every below is with filer
  draw_filter(context, canvas);
  to_draw["content_type"] === "content_img" && draw_image(context, canvas);

  // --- every below is without filer
  context.filter = "url()";
  draw_partner_color(context, canvas);
  draw_write("title", context, canvas);
  to_draw["content_type"] === "content_text" &&
    draw_write("text", context, canvas);
  to_draw["tekhne_logo"] && draw_partner_logo(canvas, context);
  draw_eu_logo(context, canvas);
}

// should be able to redraw anytime based on the above dictionnary
function draw(canvas) {
  console.log("--- draw", to_draw["mode"]);
  console.log(to_draw["filterScale"]);

  let context = canvas.getContext("2d");

  // --- clear context
  context.clearRect(0, 0, canvas.width, canvas.height);

  if (to_draw["mode"] == "logo") {
    //The backgroundColor needs to be re-added as the resizing does something weird
    context.fillStyle = to_draw["backgroundColor"];
    context.fillRect(0, 0, canvas.width, canvas.height);

    draw_logo(canvas, context);

    if (to_draw["format"] != "logo-fit") {
      draw_partner_color(context, canvas);

      // additionnal text that situate
      if (to_draw["logo"] != "tekhnē") {
        draw_write("sup_text_tl", context, canvas);
      }
      draw_write("sup_text_br", context, canvas);
      draw_eu_logo(context, canvas);
    } else {
      canvas.width = cached_logo.width;
      canvas.height = cached_logo.height;
      context.fillStyle = to_draw["backgroundColor"];
      context.fillRect(0, 0, canvas.width, canvas.height);
      cached_logo.x = 0;
      cached_logo.y = 0;
      draw_logo(canvas, context);
    }
  } else {
    //The backgroundColor needs to be re-added as the resizing does something weird
    context.fillStyle = to_draw["backgroundColor"];
    context.fillRect(0, 0, canvas.width, canvas.height);

    draw_post(canvas, context);
  }
  console.log(to_draw);
}

//  IMAGE IMPORT
//  -------------------------------------------------------------------

let image_inputs = document.getElementsByClassName("image-input");

function import_image(e) {
  let files = e.target.files;

  // FileReader support
  if (FileReader && files && files.length) {
    let fr = new FileReader();
    fr.onload = function() {
      let img = new Image();
      img.onload = function() {
        for (let canvas of canvases) {
          draw(canvas);
        }
      };
      img.src = fr.result;
      to_draw["img"] = img;
    };
    fr.readAsDataURL(files[0]);
  }
}

//  UPDATE PARTNERS
//  -------------------------------------------------------------------

let partner_inputs = document.getElementsByClassName("partner-input");

function update_partners() {
  to_draw["partners"] = [];

  for (let partner_input of partner_inputs) {
    if (partner_input.checked) {
      partner = {
        value: partner_input.value,
        id: partner_input.id.split("_")[0],
      };
      to_draw["partners"].push(partner);

      console.log("selected partners", to_draw["partners"]);

      // pick a number of filter equal to the number of letter
      // and save it
      to_draw["logo"] = partner.value;
      to_draw["filters"] = pick_filters(to_draw["logo"]);
    }
  }

  if (has_been_init) {
    for (let canvas of canvases) {
      draw(canvas);
    }
  }
}

//  UPDATE FILTER
//  -------------------------------------------------------------------
const image_filter_inputs = document.getElementsByClassName("filter-input");

function update_image_filters(image_filter_input) {
  if (image_filter_input.checked) {
    to_draw.image_filters.push(image_filter_input.name)
  } else {
    to_draw.image_filters = to_draw.image_filters.filter(filter => filter !== image_filter_input.name)
  }

  for (let canvas of canvases) {
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;

    if (has_been_init) {
      // redraw after updating dimensions
      draw(canvas);
    }
  }
}


//  UPDATE MODE
//  -------------------------------------------------------------------

let mode_inputs = document.getElementsByClassName("mode-input");

function update_mode(mode_input) {
  if (mode_input.checked) {
    let name = mode_input.name;

    // remove the others
    let other_mode_inputs = document.querySelectorAll(
      ".mode-input[name='" + name + "']"
    );
    for (let other_mode_input of other_mode_inputs) {
      if (other_mode_input != mode_input) {
        document.body.classList.remove(other_mode_input.value);
      }
    }

    // add this one
    document.body.classList.add(mode_input.value);

    // change in drawing options
    to_draw[name] = mode_input.value;

    // if we're on any other mode than logo-fit then put back background to white
    if (mode_input.value != "logo-fit") {
      to_draw["backgroundColor"] = "#f0f0f0";
    }
    if (mode_input.value === "fb-event") {
      document.getElementById("content_img").click();
    }

    for (let canvas of canvases) {
      canvas.width = canvas.offsetWidth;
      canvas.height = canvas.offsetHeight;

      if (has_been_init) {
        // redraw after updating dimensions
        draw(canvas);
      }
    }
  }
  if (mode_input.type === "range") {
    to_draw[mode_input.name] = mode_input.value
    // Update the value of the label. This is probably not the best way to do it in case there are more sliders
    document.querySelector('label[for="img_size_multiplier"]').innerText = `Image scale is: ${mode_input.value}`

    for (let canvas of canvases) {
      canvas.width = canvas.offsetWidth;
      canvas.height = canvas.offsetHeight;

      if (has_been_init) {
        // redraw after updating dimensions
        draw(canvas);
      }
    }
  }
}

//  UPDATE CANVAS
//  -------------------------------------------------------------------

let canvas_inputs = document.getElementsByClassName("canvas-input");

function update_canvas(canvas_input) {
  if (canvas_input.checked) {
    let name = canvas_input.name;

    // remove the others
    let other_mode_inputs = document.querySelectorAll(
      ".canvas-input[name='" + name + "']"
    );
    for (let other_mode_input of other_mode_inputs) {
      if (other_mode_input != canvas_input) {
        document.body.classList.remove(other_mode_input.value);
      }
    }

    // change in drawing options
    to_draw[name] = canvas_input.value;
    console.log(`changing ${name} to ${to_draw[name]}`);

    for (let canvas of canvases) {
      if (has_been_init) {
        // redraw after updating dimensions
        draw(canvas);
      }
    }
  }
}
// Supplementary text

const sup_text_inputs = document.getElementsByClassName("sup-text");

function updateCanvas() {
  for (let canvas of canvases) {
    if (has_been_init) {
      draw(canvas);
    }
  }
}
function delayedUpdate() {
  let timeout;
  const delaye = () => {
    const later = () => {
      timeout = null;
      updateCanvas();
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, 1000);
  };
  return delaye();
}

function update_sup_text(sup_text_input) {
  to_draw[sup_text_input.name] = sup_text_input.value;
  updateCanvas();
}

// Logo controls

const logo_inputs = document.getElementsByClassName("logo-input");

function update_logo(logo_input) {
  to_draw[logo_input.id] = logo_input.checked ? eu_logo : undefined;

  for (let canvas of canvases) {
    if (has_been_init) {
      draw(canvas);
    }
  }
}

//  SVG FILTERS CONTROLS
//  -------------------------------------------------------------------

let random_seed_button = document.getElementById("random-seed");
let random_pick_button = document.getElementById("random-pick");
let svg_inputs = document.getElementsByClassName("svg-input");

function random_seed() {
  console.log("changing all feTurbulence seeds");

  let primitives = document.querySelectorAll("feTurbulence");
  primitives.forEach((primitive) => {
    const seed = Math.floor(Math.random() * 1000);
    to_draw.random_seed = seed;
    primitive.setAttribute("seed", seed);
  });

  if (has_been_init) {
    for (let canvas of canvases) {
      draw(canvas);
    }
  }
}

function random_filter() {
  to_draw["filters"] = pick_filters(to_draw["logo"]);
  if (has_been_init) {
    for (let canvas of canvases) {
      draw(canvas);
    }
  }
}

function svg_prop_update(input) {
  // read current value of a slider,
  // and update it's tied prop
  //   let primitive_name = slider.dataset.primitive;
  //   let prop = slider.dataset.prop;

  // let primitives = document.getElementsByTagNameNS("http://www.w3.org/2000/svg", "feDisplacementMap");
  const primitives = document.querySelectorAll("feDisplacementMap");
  const imageDistortion = document.querySelectorAll(
    "#image_filters feDisplacementMap"
  );

  //
  to_draw[input.name] = input.value;
  if (input.name === "filterScale") {
    primitives.forEach((primitive) => {
      primitive.setAttribute("scale", input.value);
    });
  } else {
    imageDistortion.forEach((primitive) => {
      primitive.setAttribute("scale", input.value);
    });
  }

  if (has_been_init) {
    for (let canvas of canvases) {
      draw(canvas);
    }
  }
}

//  TEXT CONTENT
//  -------------------------------------------------------------------

let text_inputs = document.getElementsByClassName("text-input");

function update_text(input) {
  let text = input.value;
  let type = input.name;
  to_draw[type] = text;

  if (has_been_init) {
    for (let canvas of canvases) {
      draw(canvas);
    }
  }
}

//  INIT
//  -------------------------------------------------------------------

let has_been_init = false;

function add_slide() {
  let slide = slide_template.content.cloneNode(true);
  slides_container.append(slide);
}

function init() {
  // init first slide
  add_slide();

  for (let image_filter_input of image_filter_inputs) {
    image_filter_input.addEventListener("input", () => {
      update_image_filters(image_filter_input)
    });
    update_image_filters(image_filter_input)
  }

  // update format according to selected
  for (let mode_input of mode_inputs) {
    mode_input.addEventListener("input", function() {
      update_mode(mode_input);
    });
    update_mode(mode_input);
  }

  // init canvas inputs
  for (let canvas_input of canvas_inputs) {
    canvas_input.addEventListener("input", function() {
      update_canvas(canvas_input);
    });
    update_canvas(canvas_input);
  }
  // init supplementary text changes
  for (let sup_text_input of sup_text_inputs) {
    sup_text_input.addEventListener("input", () => {
      update_sup_text(sup_text_input);
    });
    update_sup_text(sup_text_input);
  }

  // init logo changes
  for (let logo_input of logo_inputs) {
    logo_input.addEventListener("input", () => {
      update_logo(logo_input);
    });
    update_logo(logo_input);
  }

  // init image_inut
  for (let image_input of image_inputs) {
    image_input.addEventListener("input", function(e) {
      import_image(e);
    });
  }

  // init partners changing
  for (let partner_input of partner_inputs) {
    partner_input.addEventListener("input", function() {
      update_partners();
    });
  }
  update_partners();

  // init svg sliders
  for (let input of svg_inputs) {
    input.addEventListener("input", function() {
      if (input.checked) {
        svg_prop_update(input);
      }
    });
    if (input.checked) {
      svg_prop_update(input);
    }
  }
  random_seed_button.addEventListener("click", random_seed);
  random_pick_button.addEventListener("click", random_filter);

  // init text
  for (let text_input of text_inputs) {
    text_input.addEventListener("input", function() {
      update_text(text_input);
    });
    update_text(text_input);
  }

  // draw first time
  for (let canvas of canvases) {
    draw(canvas);
  }

  // Randomize the seed on every run
  random_seed();

  has_been_init = true;
}

//  FILTERS IMPORT
filter_paths = [
  "filters/custom_filters.svg",
  "filters/image_filters.svg",
  "filters/inkscape_filters.svg",
];

function get_filter(path) {
  const xhttp = new XMLHttpRequest();
  xhttp.onload = function() {
    svg_filter = this.responseText;
    filters_container.innerHTML += svg_filter;
  };
  xhttp.open("GET", path, true);
  xhttp.send();
}

for (let filter_path of filter_paths) {
  get_filter(filter_path);
}

if (document && document.fonts) {
  // Do not block page loading
  setTimeout(function() {
    document.fonts.load("16px AmiamieRounded-bold").then(() => {
      document.fonts.load("16px AmiamieRounded").then(() => {
        init();
      });
    });
  }, 0);
}

//  SAVE FILE
//  -------------------------------------------------------------------

let save_button = document.getElementById("save-button");

save_button.addEventListener("click", function() {
  // for every slide
  for (let canvas of canvases) {
    // save the canvas
    canvas.toBlob(function(blob) {
      const filename = `tekhne_fold_${to_draw.mode}_${to_draw.format}_${to_draw.random_seed
        }_${Date.now()}.png`;
      saveAs(blob, filename);
    });
  }
});
